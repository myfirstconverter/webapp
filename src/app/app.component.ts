import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'webapp';

  currencies: any;
  from = "FROM";
  to = "TO";
  newForm: FormGroup;

  access_key = "";

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.initNewForm();
    this.getCurrencies();
  }

  getCurrencies() {
    this.http.get("http://data.fixer.io/api/symbols?access_key=" + this.access_key).subscribe((data: {symbols})=>{
      console.log("data",data);
      this.currencies = Object.keys(data.symbols);
    })
  }

  private initNewForm() {

    this.newForm = new FormGroup({
      montant: new FormControl('', [Validators.required]),
      arrivee: new FormControl('', []),
    });
  }

  get f() { return this.newForm.controls; }

  private prepareDataToSave() {
    const recordToSave = new FormData();
    
    recordToSave.append('depart', this.from);
    recordToSave.append('arrivee', this.to);
    recordToSave.append('ammount', this.f.montant.value);

    return recordToSave;
  }

  onSubmit() {
    if (this.newForm.invalid) {
      alert('pas bon')
      return;
    }
    
    const data = this.prepareDataToSave();
    
    this.api.post("convert", data).subscribe(res => {
      this.newForm.get("arrivee").setValue(res);
    })
  }
}
